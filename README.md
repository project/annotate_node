The module allows for comments/corrections/annotations on each sentence or paragraph on static nodes. Comments/annotation/corrections will appear below the content to which they refer.  If correction mode is enabled in the configuration, it will also display additions and deletions in green and red.

The module is not intended for nodes that will be edited again after receiving annotations. Adding additional paragraphs or sentences after annotations have been added will result in annotations being misplaced.

My suggestion to start is to use the Annotate Node content type first to see how it works.  Then if you if you decide to use it change the configuration to one of your own content types.  Otherwise you'll lose content if you ever decide to uninstall this module and the annotate_node content type is removed.

To set-up a content type other than annotate_node, just copy and rename the the file in /annotate_node/templates/node--annotate_node.html.twig.  Rename this file to be node--your_content_type.html.twig.  The content type also needs a body (must be the machine name) as a field.  Set the config at admin/config/annotation-configuration.

The module has some limitations:
	The module numbers each sentence or paragraph and allows annotations on each.  It isn't intended to be used to annotate a node that is still being edited.  Any changes to the document will mess-up the placement of the annotations.
	While it provides the option to break up the text by sentence, it does so by looking for a period and checking spacing.  This isn't always perfect.
	If you change the settings to break text by paragraph to sentence of vice versa, the color coding of changes done	by JavaScript will be messed up on previous comments.
	The Javascript we are using to highlight additions and deletions in correction mode does not work with asian characters.

Debugging:
	If the form doesn't close when commenting, make sure that div exists and covers all content of the node.  There's a mutationObserver in the .js file listening for changes on div where id = "content". If that's the problem and it isn't a single page, start by looking in the template provided by the module.  The html is also altered in the hook_preprocess_node.  Can debug by changing the config settings to sentences or paragraphs and seeing when the div breaks.
	If the buttons still do not appear on your content type but work on annotate_node, be sure you don't have another twig already overriding the node for that content type, likely in your theme directory.
	If nothing happens when clicking the button, check the console in your browser for javascript errors.  If there's something about popper function not defined, you theme is probably still trying to use Popper v1.  Drupal 9 has Popper v2 in core, but not all themes have been updated.